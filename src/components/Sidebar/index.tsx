import NextLink from 'next/link';
import React from 'react';

const styling = {
  buttonToggler:
    'cursor-pointer rounded border border-solid border-transparent bg-transparent px-3 py-1 text-xl leading-none text-black opacity-50 md:hidden',
  brandLink:
    'text-blueGray-600 mr-0 inline-block whitespace-nowrap p-4 px-0 text-left text-sm font-bold uppercase md:block md:pb-2',
};

const Sidebar = () => {
  const [collapseShow, setCollapseShow] = React.useState('hidden');
  return (
    <nav className="relative z-10 flex flex-wrap items-center justify-between bg-white py-4 px-6 shadow-xl md:fixed md:inset-y-0 md:left-0 md:block md:w-64 md:flex-row md:flex-nowrap md:overflow-hidden md:overflow-y-auto">
      <div className="mx-auto flex w-full flex-wrap items-center justify-between px-0 md:min-h-full md:flex-col md:flex-nowrap md:items-stretch">
        {/* Toggler */}
        <button
          className={styling.buttonToggler}
          type="button"
          onClick={() => setCollapseShow('bg-white m-2 py-3 px-6')}
        >
          <i className="fas fa-bars"></i>
        </button>
        {/* Brand */}
        <NextLink className={styling.brandLink} href="/">
          KLIKBID GEBYAR
        </NextLink>
        {/* User */}
        <ul className="flex list-none flex-wrap items-center md:hidden">
          <li className="relative inline-block">
            {/* <NotificationDropdown /> */}
          </li>
          <li className="relative inline-block">{/* <UserDropdown /> */}</li>
        </ul>
        {/* Collapse */}
        <div
          className={`md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded ${collapseShow}`}
        >
          {/* Collapse header */}
          <div className="mb-4 block border-b border-solid border-blueGray-200 pb-4 md:hidden md:min-w-full">
            <div className="flex flex-wrap">
              <div className="w-6/12">
                <NextLink
                  className="mr-0 inline-block whitespace-nowrap p-4 px-0 text-left text-sm font-bold uppercase text-blueGray-600 md:block md:pb-2"
                  href="/"
                >
                  KLIKBID GEBYAR
                </NextLink>
              </div>
              <div className="flex w-6/12 justify-end">
                <button
                  type="button"
                  className="cursor-pointer rounded border border-solid border-transparent bg-transparent px-3 py-1 text-xl leading-none text-black opacity-50 md:hidden"
                  onClick={() => setCollapseShow('hidden')}
                >
                  <i className="fas fa-times"></i>
                </button>
              </div>
            </div>
          </div>
          {/* Form */}
          <form className="mt-6 mb-4 md:hidden">
            <div className="mb-3 pt-0">
              <input
                type="text"
                placeholder="Search"
                className="h-12 w-full rounded border-0 border-solid border-blueGray-500 bg-white px-3 py-2 text-base font-normal leading-snug text-blueGray-600 shadow-none outline-none placeholder:text-blueGray-300 focus:outline-none"
              />
            </div>
          </form>

          {/* Divider */}
          <hr className="my-4 md:min-w-full" />
          {/* Heading */}
          <h6 className="block pt-1 pb-4 text-xs font-bold uppercase text-blueGray-500 no-underline md:min-w-full">
            Admin Layout Pages
          </h6>
          {/* Navigation */}

          <ul className="flex list-none flex-col md:min-w-full md:flex-col">
            <li className="items-center">
              <a
                className={`text-xs uppercase py-3 font-bold block hover:text-lightBlue-600`}
                href="/admin/dashboard"
              >
                <i className={`fas fa-tv mr-2 text-sm text-blueGray-300`}></i>{' '}
                Dashboard
              </a>
            </li>

            {/* <li className="items-center">
              <NextLink
                className={`text-xs uppercase py-3 font-bold block ${
                  window.location.href.indexOf('/admin/settings') !== -1
                    ? 'text-lightBlue-500 hover:text-lightBlue-600'
                    : 'text-blueGray-700 hover:text-blueGray-500'
                }`}
                href="/admin/settings"
              >
                <i
                  className={`fas fa-tools mr-2 text-sm ${
                    window.location.href.indexOf('/admin/settings') !== -1
                      ? 'opacity-75'
                      : 'text-blueGray-300'
                  }`}
                ></i>{' '}
                Settings
              </NextLink>
            </li>

            <li className="items-center">
              <NextLink
                className={`text-xs uppercase py-3 font-bold block ${
                  window.location.href.indexOf('/admin/tables') !== -1
                    ? 'text-lightBlue-500 hover:text-lightBlue-600'
                    : 'text-blueGray-700 hover:text-blueGray-500'
                }`}
                href="/admin/tables"
              >
                <i
                  className={`fas fa-table mr-2 text-sm ${
                    window.location.href.indexOf('/admin/tables') !== -1
                      ? 'opacity-75'
                      : 'text-blueGray-300'
                  }`}
                ></i>{' '}
                Tables
              </NextLink>
            </li>

            <li className="items-center">
              <NextLink
                className={`text-xs uppercase py-3 font-bold block ${
                  window.location.href.indexOf('/admin/maps') !== -1
                    ? 'text-lightBlue-500 hover:text-lightBlue-600'
                    : 'text-blueGray-700 hover:text-blueGray-500'
                }`}
                href="/admin/maps"
              >
                <i
                  className={`fas fa-map-marked mr-2 text-sm ${
                    window.location.href.indexOf('/admin/maps') !== -1
                      ? 'opacity-75'
                      : 'text-blueGray-300'
                  }`}
                ></i>{' '}
                Maps
              </NextLink>
            </li> */}
          </ul>

          {/* Divider */}
          <hr className="my-4 md:min-w-full" />
        </div>
      </div>
    </nav>
  );
};

export default Sidebar;
