/* eslint-disable jsx-a11y/alt-text */
import classnames from 'classnames';
import { useState } from 'react';

import Button from '../Button';
import Logo from '../Logo';
import NavSection from '../NavSection';

export default function Navbar() {
  const [offcanvas, setOffcanvas] = useState(false);

  return (
    <>
      <div className="flex items-center py-10">
        <div className="w-4/12">
          <Logo />
        </div>
        <div className="hidden w-4/12 md:block">
          <NavSection dir="horizontal" scheme="light" />
        </div>
        <div className="hidden w-3/12 text-right md:block">
          <Button href="#" pill variant="outline-blue">
            LOGIN
          </Button>
        </div>
        <div className="w-9/12 text-right md:hidden">
          <img
            src="/menu.svg"
            className="inline-block"
            onClick={() => setOffcanvas(true)}
          />
        </div>
      </div>

      <div
        className={classnames(
          'fixed top-0 z-10 h-full w-full bg-white p-10 transition-all md:hidden',
          offcanvas ? 'right-0' : '-right-full'
        )}
      >
        <img
          src="/x.svg"
          className="absolute top-8 right-8 w-8"
          onClick={() => setOffcanvas(false)}
        />
        <NavSection scheme="dark" dir="vertical" />
      </div>
    </>
  );
}
