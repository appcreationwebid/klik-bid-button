import React from 'react';

import AdminNavbar from '@/components/AdminNavbar';
import HeaderStats from '@/components/HeaderStats';
import Sidebar from '@/components/Sidebar';

type Props = {
  children: React.ReactNode;
};

const AdminLayout = (props: Props) => {
  const { children } = props;
  return (
    <React.Fragment>
      <Sidebar />
      <div className="bg-blueGray-100 relative md:ml-64">
        <AdminNavbar />
        <HeaderStats />
        <div className="-m-24 mx-auto w-full px-4 md:px-10">{children}</div>
      </div>
    </React.Fragment>
  );
};

export default AdminLayout;
