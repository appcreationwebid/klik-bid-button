import { AppConfig } from '@/utils/AppConfig';

export default function Logo() {
  return (
    <div className="font-mono text-lg font-semibold leading-relaxed text-white md:text-2xl lg:text-3xl ">
      {AppConfig.title}
    </div>
  );
}
