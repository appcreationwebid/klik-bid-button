import type { ReactNode } from 'react';

import Navbar from '@/components/Navbar';

type IMainProps = {
  meta: ReactNode;
  children: ReactNode;
};

const Main = (props: IMainProps) => (
  <div className="w-full text-gray-700 antialiased">
    {props.meta}
    {/* eslint-disable-next-line tailwindcss/no-custom-classname */}
    <div className="h-full pb-96 bg-hero">
      <div className="container mx-auto px-10 2xl:px-0">
        <Navbar />
        {props.children}
      </div>
    </div>
  </div>
);

export { Main };
