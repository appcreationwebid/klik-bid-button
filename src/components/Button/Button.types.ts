export interface Props {
  href?: string;
  className?: string;
  variant?: any;
  children?: string;
  pill?: boolean;
}
