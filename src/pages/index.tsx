/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable tailwindcss/no-custom-classname */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable prettier/prettier */

import { Meta } from "@/layouts/Meta";
import { Main } from "@/templates/Main";

const styling = {
  titleSection:
    "mx-auto text-center font-mono text-2xl font-bold md:font-semibold leading-relaxed text-white md:text-3xl lg:w-10/12 lg:text-4xl xl:w-8/12 2xl:w-6/12",
  timeBoxContainer: "mt-8 flex w-1/2 justify-center gap-2",
  timeBoxGroupContainer: "flex flex-row gap-2 p-2",
  timeBoxSection:
    "flex h-24 w-20 items-center justify-center border-4 border-gray-700 text-center shadow-inner bg-gray-800",
  timeBoxText: "font-mono text-5xl font-bold text-white",
  boxInfoContainer: 'flex h-20 w-full 2xl:mx-24 items-center border-4 border-gray-700 shadow-inner bg-gray-800',
  infoContainer: ' mx-2 md:mx-4 lg:mx-4 flex flex-col items-start',
  infoTextSection: '2xl:text-xl md:text-sm text-xs font-mono font-bold text-gray-500'
};

const _renderTimeBox = () => (
  <div className={styling.timeBoxContainer}>
    <div className={styling.timeBoxGroupContainer}>
      <div className={styling.timeBoxSection}>
        <span className={styling.timeBoxText}>5</span>
      </div>
      <div className={styling.timeBoxSection}>
        <span className={styling.timeBoxText}>9</span>
      </div>
    </div>
    <div className="flex items-center gap-1">
      <span className={`${styling.timeBoxText} animate-ping`}>:</span>
    </div>
    <div className={styling.timeBoxGroupContainer}>
      <div className={styling.timeBoxSection}>
        <span className={styling.timeBoxText}>0</span>
      </div>
      <div className={styling.timeBoxSection}>
        <span className={styling.timeBoxText}>0</span>
      </div>
    </div>
  </div>
);

const Index = () => {
  // const router = useRouter();

  return (
    <Main
      meta={
        <Meta
          title="KlikBid Button"
          description="Win attractive prizes just by clicking the button."
        />
      }
    >
      <div className="-mt-4 flex flex-col items-center md:mt-6 lg:mt-10 xl:mt-10">
        <div className="flex w-full flex-col items-center">
          <h1 className={styling.titleSection}>CLICK AND WIN PAJERO</h1>
          {_renderTimeBox()}
          <div className={'mt-4 flex w-[400px] items-center justify-center gap-2 md:w-1/2 lg:w-1/2'}>
            <div className={styling.boxInfoContainer}>
              <div className='flex w-full flex-row justify-center'>
                <div className={styling.infoContainer}>
                  <span className={styling.infoTextSection}>
                    Click Number : 20
                  </span>
                  <span className={styling.infoTextSection}>
                    Time Clicked : 52.899s
                  </span>
                </div>
                <div className={styling.infoContainer}>
                  <span className={styling.infoTextSection}>
                    Participants : 2490
                  </span>
                </div>
              </div>
            </div>
          </div>
          <img src="/assets/images/button-click-min.png" className="h-50 mt-4 w-72 hover:scale-95" />
        </div>
      </div>
    </Main>
  );
};

export default Index;
