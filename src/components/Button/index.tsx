import classnames from 'classnames';

import type { Props } from './Button.types';

export default function Button(props: Props) {
  const { href, className, variant, children, pill } = props;
  const variants = {
    'outline-blue': `border border-sky-500 text-white hover:text-white hover:bg-sky-500`,
    'outline-yellow': `border border-yellow-500 text-yellow-500 hover:text-black hover:bg-yellow-500`,
    yellow: 'bg-yellow-500 hover:bg-yellow-600 text-black',
    black: 'bg-black hover:bg-opacity-90 text-white',
  };

  const pickedVariant = variants[variant];

  return (
    <a
      href={href}
      className={classnames(
        'inline-block py-3 px-10 text-lg font-semibold transition',
        pill && 'rounded-full',
        pickedVariant,
        className
      )}
    >
      {children}
    </a>
  );
}
