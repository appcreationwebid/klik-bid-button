import classnames from 'classnames';

import type { Props } from './NavItems.types';

export default function NavItem(props: Props) {
  const { href, scheme, children } = props;
  const schemes = {
    light: 'text-white text-opacity-60 hover:text-opacity-100',
    dark: 'text-black',
  };

  const pickedScheme = schemes[scheme];

  return (
    <li>
      <a
        href={href}
        className={classnames('text-lg font-semibold transition', pickedScheme)}
      >
        {children}
      </a>
    </li>
  );
}
