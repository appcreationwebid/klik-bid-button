import classnames from 'classnames';

import NavItem from '../NavItems';

export default function NavSection({ scheme, dir }) {
  const dirs = {
    horizontal: 'justify-center space-x-10',
    vertical: 'flex-col space-y-6',
  };

  const pickedDir = dirs[dir];

  return (
    <ul className={classnames('flex', pickedDir)}>
      <div className="md:hidden">
        <NavItem scheme={scheme} href="https://facebook.com">
          Login
        </NavItem>
      </div>
      <NavItem scheme={scheme} href="#profile">
        How to Play
      </NavItem>
      <NavItem scheme={scheme} href="#skills">
        FAQ
      </NavItem>
    </ul>
  );
}
